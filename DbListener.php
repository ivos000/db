<?php
namespace libraries\db;

class DbListener
{
	protected $_profiler;

    /**
     * Creates the profiler and starts the logging
     */
    public function __construct()
    {
        $this->_profiler = new \Phalcon\Db\Profiler();
    }

    /**
     * This is executed if the event triggered is 'beforeQuery'
     */
    public function beforeQuery($event, $connection)
    {
        $this->_profiler->startProfile($connection->getSQLStatement());

        // Check for malicious words in SQL statements
	    if (preg_match('/DROP|ALTER/i', $connection->getSQLStatement())) {
	        // DROP/ALTER operations aren't allowed in the application,
	        // this must be a SQL injection!
	        return false;
	    }

	    // It's OK
	    return true;
    }

    /**
     * This is executed if the event triggered is 'afterQuery'
     */
    public function afterQuery($event, $connection)
    {
        $this->_profiler->stopProfile();

		$profile = $this->_profiler->getLastProfile();

		$info = $profile->getSQLStatement() . ';' 
			. $profile->getInitialTime() . ';' 
			. $profile->getFinalTime() . ';'
			. rtrim(sprintf('%.'.ini_get('serialize_precision').'F', $profile->getTotalElapsedSeconds()));

         \Phalcon\Di::getDefault()->getShared('logger')->get(LOG_MYSQL)->info($info);
    }
}
