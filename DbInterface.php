<?php
namespace libraries\db;

interface DbInterface
{
    /**
     * insert
     *
     * @param string $table
     * @param array  $params
     * @return insert id
     */
	public function insert($table, $params);

    /**
     * query a single row
     *
     * @param string $sql
     * @return result in 1d array
     */
    public function queryRow($sql);

    /**
     * query
     *
     * @param string $sql
     * @return result in 2d array
     */
    public function queryArray($sql);

    /**
     * update
     *
     * @param string $sql
     * @return number of affected rows
     */
    public function updateRaw($sql);

    /**
     * update
     *
     * @param string $table
     * @param array  $params - the set clause where key is the column name and value is the value
     * @param array  $constraints - the where clause where key is the column name and value is the value
     * @return number of affected rows
     */
    public function update($table, $params, $constraints);

    /**
     * delete
     *
     * @param string $sql
     * @return number of affected rows
     */
    public function delete($sql);

    /**
     * create table
     *
     * @param string $name
     * @param array  $schema - follows phalcon convention
     * @return unknown
     */
    public function createTable($name, $schema);

    /**
     * drop table
     *
     * @param string $name
     * @return unknown
     */
    public function dropTable($name);

}