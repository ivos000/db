<?php
namespace libraries\db;

class Mysql implements DbInterface
{
	public $_read;
	public $_write;

    /**
     * Mysql constructor.
     * @param array $dbReadConfig
     * @param array $dbWriteConfig
     */
	public function __construct($dbReadConfig, $dbWriteConfig = null)
	{
		$eventsManager = new \Phalcon\Events\Manager();
		$eventsManager->attach('db', new DbListener);

		$dbReadConfig['options'] = [
			\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
		];
		$this->_read = new \Phalcon\Db\Adapter\Pdo\Mysql($dbReadConfig);
		$this->_read->setEventsManager($eventsManager);


		if (is_null($dbWriteConfig)) {
			$this->_write = $this->_read;
		}
		else {
			$dbWriteConfig['options'] = [
				\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
			];
			$this->_write = new \Phalcon\Db\Adapter\Pdo\Mysql($dbWriteConfig);
			$this->_write->setEventsManager($eventsManager);
			$this->_write->setNestedTransactionsWithSavepoints(true);
		}
	}

    /**
     * internal function to handle all queries
     *
     * @param resource $db
     * @param string   $sql
     * @param array $params
     * @return result
     */
	protected function query($db, $sql, $params = array())
	{
		array_shift($params);
		$rs = $db->query($sql, $params);

		if ($db->getErrorInfo()[0] !== '00000') {
			throw new \PDOException($db->getErrorInfo()[2], $db->getErrorInfo()[0]);
		}

		return $rs;
	}

    /**
     * internal function to handle all mutation queries
     *
     * @param resource $db
     * @param string   $sql
     * @param array $params
     * @return result
     */
	protected function execute($db, $sql, $params = array())
	{
		array_shift($params);
		$rs = $db->execute($sql, $params);

		if ($db->getErrorInfo()[0] !== '00000') {
			throw new \PDOException($db->getErrorInfo()[2], $db->getErrorInfo()[0]);
		}

		return $rs;
	}

	/**
	 * query a single row
	 *
	 * @param string $sql
	 * @return result in 1d array
	 */
    public function queryRow($sql)
    {
        $rs = $this->query($this->_read, $sql . ' LIMIT 1', func_get_args());
        return $rs->fetch(\Phalcon\Db::FETCH_ASSOC); 
    }

	/**
	 * query
	 *
	 * @param string $sql
	 * @return result in 2d array
	 */
    public function queryArray($sql)
    {
        $rs = $this->query($this->_read, $sql, func_get_args());
        return $rs->fetchAll(\Phalcon\Db::FETCH_ASSOC); 
    }


	/**
	 * insert
	 *
	 * @param string $table
	 * @param array  $params
	 * @return insert id
	 */
	public function insert($table, $params)
    {
        $columns = implode('`,`', array_keys($params));
        $placeholder = array();
        for ($i = 0; $i < count($params); $i++) {
            $placeholder[] = '?';
        }
        $placeholder = implode(',', $placeholder);
         
        $sql = "INSERT INTO `$table` (`$columns`) VALUES ($placeholder)";
        $arr = array_values(array_merge(array('sql'=>$sql), $params));
         
        $stmt = $this->execute($this->_write, $sql, $arr);
  
        return $this->_write->lastInsertId();
    }

	/**
	 * update
	 *
	 * @param string $sql
	 * @return number of affected rows
	 */
    public function updateRaw($sql)
    {
        $this->execute($this->_write, $sql, func_get_args());         
        return $this->_write->affectedRows();
    }

	/**
	 * update
	 *
	 * @param string $table
	 * @param array  $params - the set clause where key is the column name and value is the value
	 * @param array  $constraints - the where clause where key is the column name and value is the value
	 * @return number of affected rows
	 */
    public function update($table, $params, $constraints)
    {
    	foreach ($params as $key => $val) {
    		$set_arr[] = "$key = ? ";
    	}
    	$set = implode(',', $set_arr);

    	foreach ($constraints as $key => $val) {
    		$where_arr[] = "$key = ? ";
    	}
    	$where = implode(' AND ', $where_arr);

    	$sql = "UPDATE $table SET " . $set . ' WHERE ' . $where;
        $this->execute($this->_write, $sql, array_values(array_merge(['dummy'],$params, $constraints)));
        return $this->_write->affectedRows();
    }


	/**
	 * delete
	 *
	 * @param string $sql
	 * @return number of affected rows
	 */
    public function delete($sql)
    {
        $this->execute($this->_write, $sql, func_get_args());         
        return $this->_write->affectedRows();
    }

	/**
	 * create table
	 *
	 * @param string $name
	 * @param array  $schema - follows phalcon convention
	 * @return unknown
	 */
    public function createTable($name, $schema)
    {
    	return $this->_write->createTable($name, null, $schema);
    }

	/**
	 * drop table
	 *
	 * @param string $name
	 * @return unknown
	 */
    public function dropTable($name)
    {
    	$this->_write->dropTable('test');
    }
}